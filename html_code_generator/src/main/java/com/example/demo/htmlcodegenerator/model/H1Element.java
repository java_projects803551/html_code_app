package com.example.demo.htmlcodegenerator.model;

public class H1Element extends HtmlElement{

    public H1Element(String name, String text, Attribute attribute) {
        super(name, text, attribute);
    }
}
