package com.example.demo.htmlcodegenerator.model;

public class HyperLinkElement extends HtmlElement{

    public HyperLinkElement(String name, String text, Attribute attribute) {
        super(name, text, attribute);
    }
}
