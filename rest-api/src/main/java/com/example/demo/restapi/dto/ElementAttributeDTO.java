package com.example.demo.restapi.dto;

public record ElementAttributeDTO(String name, String value) {
}
