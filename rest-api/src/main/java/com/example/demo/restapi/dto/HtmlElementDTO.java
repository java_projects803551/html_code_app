package com.example.demo.restapi.dto;

public record HtmlElementDTO(int parentId, String name, String text, ElementAttributeDTO elementAttributeDTO) {

}
